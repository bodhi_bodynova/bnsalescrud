[{$smarty.block.parent}]

[{if $oView->getClassName() == 'crs_crud_articles_list'
|| $oView->getClassName() == 'crs_crud_user_list'
|| $oView->getClassName() == 'crs_crud_actions2article_list'
|| $oView->getClassName() == 'crs_crud_actions_list'
|| $oView->getClassName() == 'crs_crud_artextends_list'
|| $oView->getClassName() == 'crs_crud_attribute_list'
|| $oView->getClassName() == 'crs_crud_categories_list'
|| $oView->getClassName() == 'crs_crud_contents_list'
|| $oView->getClassName() == 'crs_crud_delivery_list'
|| $oView->getClassName() == 'crs_crud_deliveryset_list'
|| $oView->getClassName() == 'crs_crud_object2select_list'
|| $oView->getClassName() == 'crs_crud_order_list'
|| $oView->getClassName() == 'crs_crud_price2article_list'
|| $oView->getClassName() == 'crs_crud_select_list'
|| $oView->getClassName() == 'crs_crud_bnsalesnews'
}]
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.1/css/all.css" integrity="sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf" crossorigin="anonymous">

[{*<link rel="stylesheet" href="//cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">*}]

<link rel="stylesheet" href="[{$oViewConf->getModuleUrl('crs_crud','out/src/css/ewald_crud.css')}]">

[{/if}]