<?php
namespace Ewald\crs_Crud\Application\Controller\Admin;

use OxidEsales\Eshop\Core\DatabaseProvider;
use OxidEsales\Eshop\Application\Controller\Admin\AdminListController;
use OxidEsales\Eshop\Core\Registry;
use stdClass;

class crs_crud_deliveryset_list extends \Ewald\crs_Crud\Application\Controller\Admin\crs_crud_majorlistcontroller {

    /**
     * Name of chosen object class (default null).
     *
     * @var string
     */
    protected $_sListClass = 'oxuser';

    /**
     * Type of list.
     *
     * @var string
     */
    protected $_sListType = 'oxuserlist';

    protected $_sClass = 'crs_crud_deliveryset_list';
    protected $_blAllowEmptyParentId = true;

    public function render()
    {

        parent::render();
        return 'crs_crud_articles_list.tpl';
    }

    /**
     * Schreibt Tabelle in CSV-Datei [TODO: Datei csv.txt zum Download bereitstellen, momentan muss man die Datei separat öffnen.]
     */
    public function csv(){
        parent::Majorcsv();
    }

    /**
     * Diese Funktion gibt die User-Reihenfolge der Spalten wieder (wird entweder aus SQL Tabelle geladen, oder initial standardmäßig geladen).
     */

    public function inputField()
    {
        $sString = ["OXID","OXACTIVE","OXACTIVEFROM","OXACTIVETO","OXTITLE","OXTITLE_1","OXTITLE_2","OXPOS","OXTIMESTAMP"];
        //$sString = ["oxactive", "oxid","oxparentid", "oxartnum", "keinabgleichmitsales", "OXSKIPDISCOUNTS", "OXPRICE", "OXTPRICE", "OXTITLE", "OXTITLE_1", "OXTITLE_2", "OXACTIVEFROM", "OXACTIVETO", "OXEAN", "OXPRICEA", "OXPRICEB", "oxpricec", "oxpriced", "oxpricee", "oxpricef", "oxbprice", "OXUNITNAME", "oxunitquantity", "OXVAT", "OXTHUMB", "OXICON", "oxpic1", "oxpic2", "oxpic3", "oxpic4", "oxpic5", "oxpic6", "oxpic7", "oxpic8", "oxpic9", "oxpic10", "oxpic11", "oxpic12", "OXSEARCHKEYS", "OXSEARCHKEYS_1", "OXSEARCHKEYS_2", "OXWEIGHT", "OXSTOCK", "OXSTOCKFLAG", "OXDELIVERY", "OXINSERT", "OXTIMESTAMP", "OXLENGTH", "OXWIDTH", "OXHEIGHT", "OXISSEARCH", "OXVARNAME", "OXVARNAME_1", "OXVARNAME_2", "OXVARSTOCK", "OXVARCOUNT", "OXVARSELECT", "OXVARSELECT_1", "OXVARSELECT_2", "OXVARMINPRICE", "OXVARMAXPRICE", "OXSHORTDESC", "OXSHORTDESC_1", "OXSHORTDESC_2", "oxsort", "oxsoldamount", "oxnonmaterial", "oxfreeshipping", "OXVENDORID", "OXMANUFACTURERID", "OXRATING", "OXRATINGCNT", "OXMINDELTIME", "OXMAXDELTIME", "OXDELTIMEUNIT", "BODYSHOWPRICE", "bearbeitet", "rabattflag", "__ID_Artikel", "bnflagbestand", "bestellcode", "keinabgleichmitendkunden", "isplayable", "FlagAngebotFahne", "AngebotFahneText", "AngebotFahneText_1", "AngebotFahneText_2", "Verpackungseinheit"];
        return parent::MajorInputField("ASRDeliveryset", $sString);
    }

    /**
     * Diese Funktion ist wichtig für den Tabellenaufbau. Abhängig von der Reihenfolge der Spalten und abhängig von
     * eventuellen Suchen wird ein Objekt zurückgegeben, sodass die Tabelle dynamisch erzeugt werden kann.
     */


    public function search(){
        //[TODO:] $sImplode = "'" . implode('","', $arrString) . "'";

        $sString = "OXID,OXACTIVE,OXACTIVEFROM,OXACTIVETO,OXTITLE,OXTITLE_1,OXTITLE_2,OXPOS,OXTIMESTAMP";
        //$sString = "oxactive,oxid,oxparentid,oxartnum,keinabgleichmitsales,OXSKIPDISCOUNTS,OXPRICE,OXTPRICE,OXTITLE,OXTITLE_1,OXTITLE_2,OXACTIVEFROM,OXACTIVETO,OXEAN,OXPRICEA,OXPRICEB,oxpricec,oxpriced,oxpricee,oxpricef,oxbprice,OXUNITNAME,oxunitquantity,OXVAT,OXTHUMB,OXICON,oxpic1,oxpic2,oxpic3,oxpic4,oxpic5,oxpic6,oxpic7,oxpic8,oxpic9,oxpic10,oxpic11,oxpic12,OXSEARCHKEYS,OXSEARCHKEYS_1,OXSEARCHKEYS_2,OXWEIGHT,OXSTOCK,OXSTOCKFLAG,OXDELIVERY,OXINSERT,OXTIMESTAMP,OXLENGTH,OXWIDTH,OXHEIGHT,OXISSEARCH,OXVARNAME,OXVARNAME_1,OXVARNAME_2,OXVARSTOCK,OXVARCOUNT,OXVARSELECT,OXVARSELECT_1,OXVARSELECT_2,OXVARMINPRICE,OXVARMAXPRICE,OXSHORTDESC,OXSHORTDESC_1,OXSHORTDESC_2,oxsort,oxsoldamount,oxnonmaterial,oxfreeshipping,OXVENDORID,OXMANUFACTURERID,OXRATING,OXRATINGCNT,OXMINDELTIME,OXMAXDELTIME,OXDELTIMEUNIT,BODYSHOWPRICE,bearbeitet,rabattflag,__ID_Artikel,bnflagbestand,bestellcode,keinabgleichmitendkunden,isplayable,FlagAngebotFahne,AngebotFahneText,AngebotFahneText_1,AngebotFahneText_2,Verpackungseinheit";

        $erg = parent::MajorSearch("oxdeliveryset","ASRDeliveryset",$sString);

        return $erg;
    }

    /**
     * Diese Funktion wird initial bei Abschicken des forms angesteuert. Sammelt alle nötigen Daten für das SQL-Query und ruft dann
     * eine weitere Funktion auf, entweder löschen oder updaten..
     */

    public function getAll(){
        parent::MajorGetAll("oxdeliveryset");
    }


    public function getToken(){
        return Registry::getConfig()->getRequestParameter('stoken');

    }

    public function getAdminSid(){
        return Registry::getConfig()->getRequestParameter('force_admin_sid');
    }

    public function replaceAll(){
        parent::MajorReplaceAll("oxdeliveryset");
    }

    /**
     * Diese Funktion schreibt jede Veränderung der Spaltenreihenfolge in die SQL-Tabelle, damit genau diese beim nächsten Login wieder geladen wird.
     */

    public function sortierung(){
        parent::MajorSortierung("ASRDeliveryset");
    }

    /**
     * Diese Funktion schreibt die Standardreihenfolge der Spalten in die SQL-Tabelle, falls der User das möchte.
     */

    public function sortStand(){
        $sString = "OXID,OXACTIVE,OXACTIVEFROM,OXACTIVETO,OXTITLE,OXTITLE_1,OXTITLE_2,OXPOS,OXTIMESTAMP";
        parent::MajorSortStand("ASRDeliveryset",$sString);
    }

    public function SpaltenMin(){
        return parent::MajorSpaltenMin("SpaltenMinDeliverySet");
    }


    /**
     *  Hier werden die minimierten Spalten mit einem SQL-Query Befehl in die DB gespeichert
     */

    public function saveSpalten(){
        parent::MajorSaveSpalten("SpaltenMinDeliverySet");

    }

    //[TODO: Finde heraus, was die untenstehenden Funktionen machen :D ]


    protected function _buildSelectString($oListObject = null)
    {
        $Grosseltern = oxNew(AdminListController::class);
        $sQ = $Grosseltern->_buildSelectString($oListObject);

        if ($sQ) {
            $sTable = getViewName("oxarticles");
            // $sQ .= " and $sTable.oxparentid = '' ";

            $sType = false;
            $sArtCat = \OxidEsales\Eshop\Core\Registry::getConfig()->getRequestParameter("art_category");
            if ($sArtCat && strstr($sArtCat, "@@") !== false) {
                list($sType, $sValue) = explode("@@", $sArtCat);
            }

            switch ($sType) {
                // add category
                case 'cat':
                    $oStr = getStr();
                    $sViewName = getViewName("oxobject2category");
                    $sInsert = "from $sTable left join {$sViewName} on {$sTable}.oxid = {$sViewName}.oxobjectid " .
                        "where {$sViewName}.oxcatnid = " . \OxidEsales\Eshop\Core\DatabaseProvider::getDb()->quote($sValue) . " and ";
                    $sQ = $oStr->preg_replace("/from\s+$sTable\s+where/i", $sInsert, $sQ);
                    break;
                // add category
                case 'mnf':
                    $sQ .= " and $sTable.oxmanufacturerid = " . \OxidEsales\Eshop\Core\DatabaseProvider::getDb()->quote($sValue);
                    break;
                // add vendor
                case 'vnd':
                    $sQ .= " and $sTable.oxvendorid = " . \OxidEsales\Eshop\Core\DatabaseProvider::getDb()->quote($sValue);
                    break;
            }
        }
        // $sQ .= " LIMIT 1000";
        return $sQ;
    }

    // Restliche ArticleList-Klasse



    /**
     * Returns array of fields which may be used for product data search
     *
     * @return array
     */
    public function getSearchFields()
    {
        $aSkipFields = ["oxblfixedprice", "oxvarselect", "oxamitemid",
            "oxamtaskid", "oxpixiexport", "oxpixiexported"];
        $oArticle = oxNew(\OxidEsales\Eshop\Application\Model\Article::class);

        return array_diff($oArticle->getFieldNames(), $aSkipFields);
    }

    /**
     * Load category list, mark active category;
     *
     * @param string $sType  active list type
     * @param string $sValue active list item id
     *
     * @return \OxidEsales\Eshop\Application\Model\CategoryList
     */
    public function getCategoryList($sType, $sValue)
    {
        /** @var \OxidEsales\Eshop\Application\Model\CategoryList $oCatTree parent category tree */
        $oCatTree = oxNew(\OxidEsales\Eshop\Application\Model\CategoryList::class);
        $oCatTree->loadList();
        if ($sType === 'cat') {
            foreach ($oCatTree as $oCategory) {
                if ($oCategory->oxcategories__oxid->value == $sValue) {
                    $oCategory->selected = 1;
                    break;
                }
            }
        }

        return $oCatTree;
    }

    /**
     * Load manufacturer list, mark active category;
     *
     * @param string $sType  active list type
     * @param string $sValue active list item id
     *
     * @return oxManufacturerList
     */
    public function getManufacturerList($sType, $sValue)
    {
        $oMnfTree = oxNew(\OxidEsales\Eshop\Application\Model\ManufacturerList::class);
        $oMnfTree->loadManufacturerList();
        if ($sType === 'mnf') {
            foreach ($oMnfTree as $oManufacturer) {
                if ($oManufacturer->oxmanufacturers__oxid->value == $sValue) {
                    $oManufacturer->selected = 1;
                    break;
                }
            }
        }

        return $oMnfTree;
    }

    /**
     * Load vendor list, mark active category;
     *
     * @param string $sType  active list type
     * @param string $sValue active list item id
     *
     * @return oxVendorList
     */
    public function getVendorList($sType, $sValue)
    {
        $oVndTree = oxNew(\OxidEsales\Eshop\Application\Model\VendorList::class);
        $oVndTree->loadVendorList();
        if ($sType === 'vnd') {
            foreach ($oVndTree as $oVendor) {
                if ($oVendor->oxvendor__oxid->value == $sValue) {
                    $oVendor->selected = 1;
                    break;
                }
            }
        }

        return $oVndTree;
    }

    /**
     * Builds and returns array of SQL WHERE conditions.
     *
     * @return array
     */
    public function buildWhere()
    {
        // we override this to select only parent articles
        $this->_aWhere = parent::buildWhere();

        // adding folder check
        $sFolder = \OxidEsales\Eshop\Core\Registry::getConfig()->getRequestParameter('folder');
        if ($sFolder && $sFolder != '-1') {
            $this->_aWhere[getViewName("oxuser") . ".oxfolder"] = $sFolder;
        }

        return $this->_aWhere;
    }

    /**
     * Deletes entry from the database
     */
    public function deleteEntry()
    {
        $sOxId = $this->getEditObjectId();
        $oArticle = oxNew(\OxidEsales\Eshop\Application\Model\Article::class);
        if ($sOxId && $oArticle->load($sOxId)) {
            parent::deleteEntry();
        }
    }


    protected function _setListNavigationParams()
    {
        $ascdesc = Registry::getConfig()->getRequestRawParameter('asc');
        $this->asc[0] = $ascdesc;

        // list navigation
        $showNavigation = false;
        $adminListSize = $this->_getViewListSize();

        $styleTA = Registry::getConfig()->getRequestRawParameter('styleTA');
        $this->styleTA = $styleTA;
        /**
         * pageNavigation->pages wird überschrieben mit Hilfe des querys, was zur Suche aufgebaut wird.
         */

        $oDb = DatabaseProvider::getDb(DatabaseProvider::FETCH_MODE_ASSOC);



        $Anzahl1 = Registry::getConfig()->getRequestRawParameter('AnzahlSuche');


        $suche1 = Registry::getConfig()->getRequestRawParameter('suche1');
        $Spalte1 = Registry::getConfig()->getRequestRawParameter('1');

        $actPage = Registry::getConfig()->getRequestRawParameter('actPage');
        $this->actPage[0] = intval($actPage);

        //echo( 'actPage:    '   .   $actPage . "<br>");


        //echo('Suche:  ' . $suche1 . '    und Spalte: ' . $Spalte1 . "<br>");
        $this->anzahl[0] = $Anzahl1;

        $suche1 = str_replace(' ','%',$suche1);

        if(empty($Spalte1)) {
            $Spalte1 = '""';
        } else{
            if(!empty($suche1)){
                $queryCount = "SELECT COUNT('oxid') FROM oxdeliveryset  WHERE " . $Spalte1 ." LIKE '%" . $suche1 . "%'";
                $this->suche[0] = $suche1;
                $this->spalte[0] = $Spalte1;
            } else {
                $suche1 = '""';
            }
        }

        //echo($queryCount . "<br>");



        $variable = 'Suche';
        $spalte = 'Spalte';

        for($i=2;$i<=$Anzahl1;$i++){

            $variable{$i} = Registry::getConfig()->getRequestRawParameter("suche$i");
            $variable{$i} = str_replace(' ','%',$variable{$i});
            $spalte{$i} = Registry::getConfig()->getRequestRawParameter("$i");

            if(empty($spalte{$i})) {
                if(!$queryCount){
                    //$queryCount = "SELECT COUNT('oxid') FROM oxarticles WHERE " . $variable{$i} ." LIKE '%" . $spalte{$i} . "%'";
                }
                $spalte{$i} = '""';
            } else{
                if(!empty($variable{$i})) {
                    if(!$queryCount){
                        $queryCount = "SELECT COUNT('oxid') FROM oxdeliveryset WHERE " . $spalte{$i} ." LIKE '%" . $variable{$i} . "%'";
                    } else {
                        $queryCount .= " OR " . $spalte{$i} . " LIKE '%" . $variable{$i} . "%'";
                    }
                    $this->suche[$i-1] = $variable{$i};
                    $this->spalte[$i-1] = $spalte{$i};
                } else {
                    $variable{$i} = '""';
                }
            }

            //echo($queryCount);


            // $queryCount .= " OR " .$spalte{$i} . " LIKE '%" . $variable{$i} . "%'";

            //echo(' Suche: ' . $variable{$i} . ' und Spalte: ' . $spalte{$i} . "<br>");
        }

        if(!$queryCount){
            $queryCount = "SELECT COUNT('oxid') FROM oxdeliveryset";
        }

        $zahl = $oDb->getOne($queryCount);
        if($zahl > 10000){
            $zahl = 10000;
        }
        //echo($queryCount . '       Anzahl:    ' . $zahl);
        /**
         *
         */
        $AnzahlErgebnisse = Registry::getConfig()->getRequestRawParameter('AnzahlErgebnisse');

        $email = Registry::getConfig()->getUser();
        $email = $email->oxuser__oxusername->rawValue;
        try{
            $oDbN = DatabaseProvider::getDb(DatabaseProvider::FETCH_MODE_NUM);
            $queryAdminZeilen = "SELECT AdminZeilen FROM `oxuser` WHERE OXUSERNAME = '" .$email ."'";

            $AnzahlErgebnisse = $oDbN->getAll($queryAdminZeilen);
            $sVar = $AnzahlErgebnisse[0];
        } catch(\Exception $e){
            return 'Exception abgefangen: ' . $e->getMessage() . "\n";
        }



        if($sVar[0] == ''){
            $sVar[0] = 10;
            $this->anzahlErgebnisse[0] = $sVar[0];
        }
        $this->anzahlErgebnisse[0] = $sVar[0];
        if ($this->_iListSize > $adminListSize) {
            // yes, we need to build the navigation object
            $pageNavigation = new stdClass();

            $pageNavigation->pages = round((($zahl) / $sVar[0]) + 0.4999999, 0);

            $pageNavigation->actpage = $actPage+1;


            $pageNavigation->lastlink = ($pageNavigation->pages - 1) * $adminListSize;
            $pageNavigation->nextlink = null;
            $pageNavigation->backlink = null;

            $position = $this->_iCurrListPos + $adminListSize;
            if ($position < $this->_iListSize) {
                $pageNavigation->nextlink = $position = $this->_iCurrListPos + $adminListSize;
            }

            if (($this->_iCurrListPos - $adminListSize) >= 0) {
                $pageNavigation->backlink = $position = $this->_iCurrListPos - $adminListSize;
            }

            // calculating list start position
            $start = $pageNavigation->actpage - 5;
            $start = ($start <= 0) ? 1 : $start;

            // calculating list end position
            $end = $pageNavigation->actpage + 5;
            $end = ($end < $start + 10) ? $start + 10 : $end;
            $end = ($end > $pageNavigation->pages) ? $pageNavigation->pages : $end;

            // once again adjusting start pos ..
            $start = ($end - 10 > 0) ? $end - 10 : $start;
            $start = ($pageNavigation->pages <= 11) ? 1 : $start;

            // navigation urls
            for ($i = $start; $i <= $end; $i++) {
                $page = new stdclass();
                $page->selected = 0;
                if ($i == $pageNavigation->actpage) {
                    $page->selected = 1;
                }
                $pageNavigation->changePage[$i] = $page;
            }

            $this->_aViewData['pagenavi'] = $pageNavigation;

            if (isset($this->_iOverPos)) {
                $position = $this->_iOverPos;
                $this->_iOverPos = null;
            } else {
                $position = \OxidEsales\Eshop\Core\Registry::getConfig()->getRequestParameter('lstrt');
            }

            if (!$position) {
                $position = 0;
            }

            $this->_aViewData['lstrt'] = $position;
            $this->_aViewData['listsize'] = $this->_iListSize;
            $showNavigation = true;
        }

        // determine not used space in List
        $listSizeToShow = $this->_iListSize - $this->_iCurrListPos;
        $adminListSize = $this->_getViewListSize();
        $notUsed = $adminListSize - min($listSizeToShow, $adminListSize);
        $space = $notUsed * 15;


        if (!$showNavigation) {
            $space += 20;
        }

        $this->_aViewData['iListFillsize'] = $space;
    }


}